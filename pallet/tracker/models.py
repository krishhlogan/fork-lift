from django.db import models
from datetime import datetime

# Create your models here.
class Truck(models.Model):
    STATUS=(('ON','ON'),('OFF','OFF'))
    name=models.CharField(max_length=50,unique=True)
    status=models.CharField(max_length=10,choices=STATUS,default='OFF')
    last_on_time=models.DateTimeField(null=True,blank=True)
    last_off_time=models.DateTimeField(null=True,blank=True)
    working=models.TimeField(null=True,blank=True)

    class Meta:
        db_table='Truck'
    def __str__(self):
        return self.name