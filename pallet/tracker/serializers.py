from rest_framework import serializers
from .models import Truck
from rest_framework.validators import UniqueValidator


class TruckSerializer(serializers.Serializer):
    STATUS=(('ON','ON'),('OFF','OFF'))
    name=serializers.CharField(max_length=50,validators=[UniqueValidator(Truck.objects.all())])
    status=serializers.ChoiceField(choices=STATUS,default='off',required=False)
    last_on_time=serializers.DateTimeField(allow_null=True,required=False)
    last_off_time = serializers.DateTimeField(allow_null=True,required=False)
    working=serializers.TimeField(allow_null=True,required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Truck.objects.create(**validated_data)

    def update(self, instance, validated_data):
        #print("self>>>>>>>>>>>>>>>>>>>",self)
        print("instance>>>>>>>>>>>>>>>",instance.status)
        print("validated data>>>>>>>>>>>>>>>>>",validated_data)
        instance.status=validated_data.get('status')
        if(instance.status=="ON"):
            instance.last_on_time=validated_data.get('last_on_time')
        else:
            instance.last_off_time=validated_data.get('last_off_time')
        instance.save()
        return instance
