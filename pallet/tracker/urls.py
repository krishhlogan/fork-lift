from django.urls import path,include
from django.conf.urls import url
from . import views
from django.contrib.auth.views import auth_login

urlpatterns=[
url('create',views.CreateTruck.as_view(),name="create"),
url('list',views.ListTrucks.as_view(),name="create"),
url('^update/(?P<name>[\w-]+)$',views.UpdateTruck.as_view(),name="update"),
path('login/',views.login,name='login'),
path('logout/',views.log_out,name='logout'),
path('',views.home,name="home"),
]

