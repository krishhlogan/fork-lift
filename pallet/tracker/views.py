from django.http import HttpResponse
from rest_framework import generics
from .serializers import TruckSerializer
from rest_framework.response import Response
from .models import Truck
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib.auth import logout
from rest_framework.authtoken.models import Token
import json
from datetime import datetime,timedelta
import pandas as pd
import datetime as dt
import os



def home(request):
    '''
    Home function that acts as a home page
    :param request: http request parameter
    :return: HttpResponse
    '''
    return HttpResponse("<h1>Home page</h1>")


@csrf_exempt
def login(request):
    '''
    Login function that  allows users to login
    :param request: http request parameter
    :return: JsonResponse
    '''
    if request.method=="GET":
        print("INSIDE GET>>>>>>>>>>>>>>>>>>>>>>>>")

    if request.method == "POST":
        received_data = json.loads(request.body)
        print("REceived Data>>" + str(received_data))
        username = received_data['username']
        password = received_data['password']
        if username == "" or password == "":
            return JsonResponse({"status": "Please Enter username and password"})
        if username is not None or password is not None:
            user = authenticate(username=username, password=password)
            if user is not None:
                token, _ = Token.objects.get_or_create(user=user)
                return JsonResponse({"status": "logged In", "user": username, "token": token.key})
            else:
                return JsonResponse({"status": "Cant Log in"})
        else:
            return JsonResponse({"status": "Enter User name and password"})
    else:
        return JsonResponse({"status": "No Post"})


@csrf_exempt
def log_out(request):
    """
    Logout function that logs out a user
    :param request: HttpRequest object
    :return: JsonResponse
    """
    try:
        logout(request)
        return JsonResponse({"status": "logged out"})
    except Exception as e:
        return JsonResponse({"error": e})


def create(request):
    """
    Create function for testing create url
    :param request: HttpRequest object
    :return: HttpResponse
    """
    return HttpResponse("<h1>Its Create page</h1>")


def update(request):
    """
        Update function for testing update url
        :param request: HttpRequest object
        :return: HttpResponse
        """
    return HttpResponse("<h1>Its Update page</h1>")


class CreateTruck(generics.CreateAPIView):
    serializer_class = TruckSerializer
    queryset = Truck.objects.all()

    def perform_create(self, serializer):
        """
            perform_create function that accepts serializer object and self as arguments to add new records on database
            :param serializer: Object of type seriaizer class
            :return: No return type
        """
        serializer.save()


class UpdateTruck(generics.UpdateAPIView):
    queryset = Truck.objects.all()
    lookup_field = 'name'
    serializer_class = TruckSerializer

    def partial_update(self, request, *args, **kwargs):
        """
        Partial Update function used to update specific column values using Patch request
        :param request: HttpRequest object which should be of PATCH request
        :param args: args does not contain anything
        :param kwargs:kwargs contains the truck info and partial=True
        :return: returns the serialised data that was updated
        """
        print("KWARGS>>>>>>>>>>",request.method)
        working=dt.timedelta(0,0,0)
        try:
            prev=Truck.objects.get(name=kwargs['name'])
        except Exception as e:
            return Response({"status":"No such truck exists","exception":str(e)})
        if(prev.status==request.data['status']):
            return JsonResponse({"status":"Already the same"})
        else:
            if(request.data['status']=='OFF'):
                if not os.path.exists(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt"):
                    open(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt","w").close()
                    print("File created for first off>>>>>>>")
                try:
                    data = pd.read_csv(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt", sep="\t", header=None)
                    print(data)

                    if len(data) > 0:
                        for index, row in data.iterrows():
                            s = row[2].split("+")[0]
                            row[2] = datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
                        print("Req type", type(request.data['last_off_time']), "  data iloc type  ",type(data.iloc[len(data) - 1][2]))
                        temp=datetime.strptime(request.data['last_off_time'],"%Y-%m-%d %H:%M:%S")
                        print("Req type", type(temp), "  data iloc type  ",type(data.iloc[len(data) - 1][2]))
                        try:
                            print("Type ",data.iloc[len(data) - 1][3])
                            t=datetime.strptime(data.iloc[len(data) - 1][3], "%H:%M:%S").time()
                            x=timedelta(hours=t.hour,minutes=t.minute,seconds=t.second)
                            print("time t is    ",temp,"\t",data.iloc[len(data) - 1][2],"\t",x)
                            print("subtracted time is    ",temp-data.iloc[len(data) - 1][2])

                            working = x + (temp - data.iloc[len(data) - 1][2])
                            print("Working hrs is   ",working)
                        except Exception as e:
                            print("Inner try    ",e)
                except Exception as e:
                    print("Exception occured ",e)

                #request.data['last_off_time']=datetime.now()
                print("Updated date time is >>>>>>>",request.data)
                truck=Truck.objects.get(name=kwargs['name'])
                print("Truck working hrs is >>>>",truck.name)
                serializer = TruckSerializer(Truck.objects.get(name=kwargs['name']),data=request.data, partial=True)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                truck=Truck.objects.get(name=kwargs['name'])
                with open(truck.name+"_"+str(datetime.now().date())+".txt","a") as f:
                    f.write("%s\t%s\t%s\t%s\n"%(truck.name,truck.status,str(truck.last_off_time),str(working)))
                    f.close()
                return Response(serializer.data)
            else:
                if not os.path.exists(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt"):
                    open(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt","w").close()
                    print("File created for first on>>>>>>>")
                try:
                    data = pd.read_csv(kwargs['name'] + "_" + str(datetime.now().date()) + ".txt", sep="\t", header=None)
                    print(data)
                    if len(data) > 0:
                        for index, row in data.iterrows():
                            s = row[2].split("+")[0]
                            row[2] = datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
                        working = data.iloc[len(data) - 1][3]
                except Exception as e:
                    print("Exception occured    ",e)
                print("Updated date time is >>>>>>>", request.data)
                serializer = TruckSerializer(Truck.objects.get(name=kwargs['name']), data=request.data, partial=True)
                serializer.is_valid(raise_exception=True)
                truck=Truck.objects.get(name=kwargs['name'])
                serializer.save()
                truck=Truck.objects.get(name=kwargs['name'])

                with open(truck.name+"_"+str(datetime.now().date())+".txt","a") as f:
                    f.write("%s\t%s\t%s\t%s\n"%(truck.name,truck.status,str(truck.last_on_time),str(working)))
                    f.close()
                truck.working=working
                truck.save()
                print("Serialiser data>>>>>>>>",serializer.data)
                return Response(serializer.data)


class ListTrucks(generics.ListAPIView):
    serializer_class = TruckSerializer
    print("list generics called")
    queryset = Truck.objects.all()


